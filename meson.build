project('Agar', 'cpp',
        version: '0.1.0',
        default_options: ['cpp_std=c++17',
                          'warning_level=3',
                          'b_ndebug=if-release']) 

agar_src = files(
    'src/common/events/ApplicationEvent.cpp',
    'src/common/events/Event.cpp',
    'src/common/events/KeyEvent.cpp',
    'src/common/events/MouseEvent.cpp',
    'src/common/game/Food.cpp',
    'src/common/game/Game.cpp',
    'src/common/game/GameRenderer.cpp',
    'src/common/game/PlayableRange.cpp',
    'src/common/game/Player.cpp',
    'src/common/game/PlayerController.cpp',
    'src/common/renderer/Buffer.cpp',
    'src/common/renderer/OrthographicCamera.cpp',
    'src/common/renderer/OrthographicCameraController.cpp',
    'src/common/renderer/Renderer.cpp',
    'src/common/renderer/Shader.cpp',
    'src/common/renderer/VertexArray.cpp',
    'src/common/Connection.cpp',
    'src/common/FilePathConfiguration.cpp',
    'src/common/Input.cpp',
    'src/common/Message.cpp',
    'src/common/Random.cpp',
    'src/common/ResponseQueue.cpp',
    'src/common/Timestep.cpp',
    'src/common/TimeRange.cpp',
    'src/common/Vector.cpp',
    'src/common/Window.cpp'
)

agar_include = include_directories('src/common')
client_include = include_directories('src/client')
server_include = include_directories('src/server')

client_src = files('src/client/Client.cpp',
                   'src/client/Configuration.cpp',
                   'src/client/ClientRenderer.cpp')
server_src = files('src/server/Server.cpp', 'src/server/ServerRenderer.cpp')

boost_dep  = dependency('boost', version: '>=1.76.0',
                         modules: ['program_options', 'serialization'])
glfw_dep   = dependency('glfw3', version: '>=3.3.0')
glew_dep   = dependency('glew', version: '>=2.2.0')
glm_dep    = dependency('glm', version: '>=0.9.0',
                        fallback: ['glm', 'glm_dep'])
fmt_dep    = dependency('fmt', version: '>=7.0.0',
                        fallback: ['fmt', 'fmt_dep'])
imgui_dep  = dependency('imgui', version: '>=1.79',
                        fallback: ['imgui', 'imgui_dep'])
thread_dep = dependency('threads')

agar_deps = [boost_dep, glfw_dep, glew_dep, glm_dep,
             fmt_dep, imgui_dep, thread_dep]

libagar = static_library('Agar', agar_src,
                         include_directories: agar_include,
                         dependencies: agar_deps)

agar_dep = declare_dependency(link_with: libagar,
                              include_directories: agar_include, 
                              dependencies: agar_deps)

client_exe = executable('Client', client_src,
                        include_directories: client_include,
                        dependencies: agar_dep)

server_exe = executable('Server', server_src,
                        include_directories: server_include,
                        dependencies: agar_dep)

#run_target('sway-float',
           #command: ['swaymsg',
                     #'for_window',
                     #'\'[title="Agar"]\'',
                     #'floating enable'])

run_target('run',
           command: ['res/run.sh', '@BUILD_ROOT@', '@SOURCE_ROOT@'],
           depends: [client_exe, server_exe])
