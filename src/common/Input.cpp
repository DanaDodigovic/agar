#include "Input.hpp"

Input::Input(const Window& window) noexcept : window(window) {}

bool Input::is_pressed(Key key) const noexcept
{
    GLFWwindow* w = window.get_native_handle();
    auto state    = glfwGetKey(w, static_cast<int>(key));
    return state == GLFW_PRESS || state == GLFW_REPEAT;
}

bool Input::is_pressed(MouseButton button) const noexcept
{
    GLFWwindow* w = window.get_native_handle();
    auto state    = glfwGetMouseButton(w, static_cast<int>(button));
    return state == GLFW_PRESS;
}

double Input::get_mouse_x() const noexcept
{
    auto [x, y] = get_mouse_position();
    return x;
}

double Input::get_mouse_y() const noexcept
{
    auto [x, y] = get_mouse_position();
    return y;
}

std::pair<double, double> Input::get_mouse_position() const noexcept
{
    GLFWwindow* w = window.get_native_handle();
    std::pair<double, double> pos;
    glfwGetCursorPos(w, &pos.first, &pos.second);
    return pos;
}
