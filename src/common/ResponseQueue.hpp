#ifndef RESPONSE_QUEUE_HPP
#define RESPONSE_QUEUE_HPP

#include "Connection.hpp"

#include <queue>
#include <string>
#include <memory>

class ResponseQueue {
  private:
    std::shared_ptr<Connection> connection = nullptr;
    bool write_currently_registered        = false;
    std::queue<std::string> entries;

  public:
    void set_connection(std::shared_ptr<Connection> connection) noexcept;
    void register_write(std::string message);

  private:
    void register_write();
};

#endif

