#include "renderer/Buffer.hpp"

#include <GL/glew.h>

#include "Assert.hpp"

std::size_t shader_data_type_size(ShaderDataType type) noexcept
{
    switch (type) {
        case ShaderDataType::Float: return 4;
        case ShaderDataType::Float2: return 4 * 2;
        case ShaderDataType::Float3: return 4 * 3;
        case ShaderDataType::Float4: return 4 * 4;
        case ShaderDataType::Mat3: return 4 * 3 * 3;
        case ShaderDataType::Mat4: return 4 * 4 * 4;
        case ShaderDataType::Int: return 4;
        case ShaderDataType::Int2: return 4 * 2;
        case ShaderDataType::Int3: return 4 * 3;
        case ShaderDataType::Int4: return 4 * 4;
        case ShaderDataType::Bool: return 1;
        case ShaderDataType::None:
            ASSERT_UNREACHABLE("'None' shader data type is not allowed!");
    }
    ASSERT_UNREACHABLE("Unhandled shader data type.");
}

BufferElement::BufferElement(ShaderDataType type,
                             std::string name,
                             bool normalized) noexcept
    : name(std::move(name)),
      type(type),
      size(shader_data_type_size(type)),
      normalized(normalized)
{}

unsigned BufferElement::get_component_count() const noexcept
{
    switch (type) {
        case ShaderDataType::Float: return 1;
        case ShaderDataType::Float2: return 2;
        case ShaderDataType::Float3: return 3;
        case ShaderDataType::Float4: return 4;
        case ShaderDataType::Mat3: return 3 * 3;
        case ShaderDataType::Mat4: return 4 * 4;
        case ShaderDataType::Int: return 1;
        case ShaderDataType::Int2: return 2;
        case ShaderDataType::Int3: return 3;
        case ShaderDataType::Int4: return 4;
        case ShaderDataType::Bool: return 1;
        case ShaderDataType::None:
            ASSERT_UNREACHABLE("'None' shader data type is not allowed!");
    }
    ASSERT_UNREACHABLE("Unhandled shader data type.");
}

BufferLayout::BufferLayout(
    std::initializer_list<BufferElement> elements) noexcept
    : elements(std::move(elements))
{
    calculate_offsets_and_stride();
}

const std::vector<BufferElement>& BufferLayout::get_elements() const noexcept
{
    return elements;
}

unsigned BufferLayout::get_stride() const noexcept
{
    return stride;
}

std::vector<BufferElement>::iterator BufferLayout::begin() noexcept
{
    return elements.begin();
}

std::vector<BufferElement>::iterator BufferLayout::end() noexcept
{
    return elements.end();
}

std::vector<BufferElement>::const_iterator BufferLayout::begin() const noexcept
{
    return elements.begin();
}

std::vector<BufferElement>::const_iterator BufferLayout::end() const noexcept
{
    return elements.end();
}

void BufferLayout::calculate_offsets_and_stride() noexcept
{
    unsigned offset = 0;
    stride          = 0;
    for (auto& element : elements) {
        element.offset = offset;
        offset += element.size;
        stride += element.size;
    }
}

VertexBuffer::VertexBuffer(const float* vertices,
                           unsigned count,
                           BufferLayout layout) noexcept
    : layout(std::move(layout))
{
    glCreateBuffers(1, &renderer_id);
    glBindBuffer(GL_ARRAY_BUFFER, renderer_id);

    // If no vertices are provided (memory is only allocated, not initalized),
    // dynamic vertex buffer is created.
    glBufferData(GL_ARRAY_BUFFER,
                 count * sizeof(float),
                 vertices,
                 vertices ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
}

VertexBuffer::~VertexBuffer() noexcept
{
    if (renderer_id) { glDeleteBuffers(1, &renderer_id); }
}

VertexBuffer::VertexBuffer(VertexBuffer&& other) noexcept
    : renderer_id(other.renderer_id), layout(std::move(other.layout))
{
    other.renderer_id = 0;
}

VertexBuffer& VertexBuffer::operator=(VertexBuffer&& other) noexcept
{
    if (renderer_id) { glDeleteBuffers(1, &renderer_id); }

    renderer_id = other.renderer_id;
    layout      = std::move(other.layout);

    other.renderer_id = 0;

    return *this;
}

const BufferLayout& VertexBuffer::get_layout() const noexcept
{
    return layout;
}

void VertexBuffer::update_buffer_data(const float* vertices,
                                      unsigned count) const noexcept
{
    glBufferSubData(GL_ARRAY_BUFFER, 0, count * sizeof(float), vertices);
}

void VertexBuffer::bind() const noexcept
{
    glBindBuffer(GL_ARRAY_BUFFER, renderer_id);
}

void VertexBuffer::unbind() const noexcept
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

IndexBuffer::IndexBuffer(const unsigned* indices, unsigned count) noexcept
    : count(count)
{
    glCreateBuffers(1, &renderer_id);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer_id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 count * sizeof(unsigned),
                 indices,
                 GL_STATIC_DRAW);
}

IndexBuffer::IndexBuffer(IndexBuffer&& other) noexcept
    : renderer_id(other.renderer_id), count(other.count)
{
    other.renderer_id = 0;
}

IndexBuffer& IndexBuffer::operator=(IndexBuffer&& other) noexcept
{
    if (renderer_id) { glDeleteBuffers(1, &renderer_id); }

    renderer_id = other.renderer_id;
    count       = other.count;

    other.renderer_id = 0;

    return *this;
}

IndexBuffer::~IndexBuffer() noexcept
{
    if (renderer_id) { glDeleteBuffers(1, &renderer_id); }
}

unsigned IndexBuffer::get_count() const noexcept
{
    return count;
}

void IndexBuffer::bind() const noexcept
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer_id);
}

void IndexBuffer::unbind() const noexcept
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
