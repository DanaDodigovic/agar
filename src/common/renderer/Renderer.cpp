#include "renderer/Renderer.hpp"
#include "Assert.hpp"

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>

Renderer::Renderer(const FilePathConfiguration& path_config)
{
    initialize_shader_library(path_config);
    initialize_batch_renderer();
}

void Renderer::on_window_resize(unsigned width, unsigned height) const noexcept
{
    glViewport(0, 0, width, height);
}

void Renderer::begin_scene(const OrthographicCamera& camera) noexcept
{
    { // Set oval shader view projection.
        auto& flat_color_shader = shader_library.get("flat_color_shader");
        flat_color_shader.bind();
        flat_color_shader.set_uniform_mat4("u_view_projection",
                                           camera.get_view_projection_matrix());
    }

    { // Set oval shader view projection.
        auto& oval_shader = shader_library.get("oval_shader");
        oval_shader.bind();
        oval_shader.set_uniform_mat4("u_view_projection",
                                     camera.get_view_projection_matrix());
    }
}

void Renderer::draw_quad(const glm::vec2& position,
                         const glm::vec2& size,
                         const glm::vec4& color,
                         bool fill) noexcept
{
    draw_quad({position.x, position.y, 0.0f}, size, color, fill);
}

void Renderer::draw_quad(const glm::vec3& position,
                         const glm::vec2& size,
                         const glm::vec4& color,
                         bool fill) noexcept
{
    ASSERT_UNREACHABLE("Not yet compatible with batch rendering.");

    auto& flat_color_shader = shader_library.get("flat_color_shader");
    flat_color_shader.bind();
    flat_color_shader.set_uniform4f("u_color", color);

    glm::mat4 transform = glm::translate(glm::mat4(1.0f), position) *
                          glm::scale(glm::mat4(1.0f), {size.x, size.y, 1.0f});
    flat_color_shader.set_uniform_mat4("u_transform", transform);

    vertex_array.bind();

    glDrawElements(fill ? GL_TRIANGLES : GL_LINE_LOOP,
                   vertex_array.get_index_buffer().get_count(),
                   GL_UNSIGNED_INT,
                   nullptr);
}

void Renderer::draw_circle(const glm::vec2& center,
                           float radius,
                           const glm::vec4& color) noexcept
{
    draw_circle({center.x, center.y, 0.0f}, radius, color);
}

void Renderer::draw_circle(const glm::vec3& center,
                           float radius,
                           const glm::vec4& color) noexcept
{
    if (batch_data.index_count >= max_index_count) {
        end_batch();
        begin_batch();
    }

    glm::vec3 position = {center.x - radius, center.y - radius, center.z};

    // Shorter aliases.
    auto& it       = batch_data.quad_buffer_it;
    const auto& p  = position;
    float diameter = 2 * radius;

    it->position = p;
    it->color    = color;
    it->center   = center;
    it->radius   = radius;
    ++it;

    it->position = {p.x + diameter, p.y, p.z};
    it->color    = color;
    it->center   = center;
    it->radius   = radius;
    ++it;

    it->position = {p.x + diameter, p.y + diameter, p.z};
    it->color    = color;
    it->center   = center;
    it->radius   = radius;
    ++it;

    it->position = {p.x, p.y + diameter, p.z};
    it->color    = color;
    it->center   = center;
    it->radius   = radius;
    ++it;

    batch_data.index_count += 6; // Six indices are required to draw a quad.
}

void Renderer::set_clear_color(const glm::vec4& color) const noexcept
{
    glClearColor(color.r, color.g, color.b, color.a);
}

void Renderer::clear() const noexcept
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::begin_batch() noexcept
{
    batch_data.quad_buffer_it = batch_data.quad_buffer.begin();
}

void Renderer::end_batch() noexcept
{
    std::size_t n_vertices = std::distance(batch_data.quad_buffer.begin(),
                                           batch_data.quad_buffer_it);
    auto& vertex_buffer    = vertex_array.get_vertex_buffers().front();
    vertex_buffer.update_buffer_data(&batch_data.quad_buffer.front().position.x,
                                     n_vertices * sizeof(Vertex) /
                                         sizeof(float));
    flush();
}

void Renderer::initialize_shader_library(
    const FilePathConfiguration& path_config)
{
    std::string shader   = path_config.shader.string();
    std::string fvshader = shader + "FlatColorVertex.glsl";
    std::string ffshader = shader + "FlatColorFragment.glsl";
    std::string ovshader = shader + "OvalVertex.glsl";
    std::string ofshader = shader + "OvalFragment.glsl";

    fmt::print("{} {} {} {}\n", fvshader, ffshader, ovshader, ofshader);

    shader_library.add("flat_color_shader", ShaderProgram(fvshader, ffshader));
    shader_library.add("oval_shader", ShaderProgram(ovshader, ofshader));
}

void Renderer::initialize_batch_renderer() noexcept
{
    // Allocate memory for batch on the CPU side.
    batch_data.quad_buffer.resize(max_vertex_count);
    batch_data.quad_buffer_it = batch_data.quad_buffer.begin();

    // Allocate memory for batch on the GPU side.
    VertexBuffer vb(nullptr,
                    max_vertex_count * sizeof(Vertex) / sizeof(float),
                    {{ShaderDataType::Float3, "position"},
                     {ShaderDataType::Float3, "center"},
                     {ShaderDataType::Float, "radius"},
                     {ShaderDataType::Float4, "color"}});
    vertex_array.add_vertex_buffer(std::move(vb));

    // Initialize index buffer.
    std::vector<unsigned> indices(max_index_count);
    unsigned offset = 0;
    for (std::size_t i = 0; i < max_index_count; i += 6) {
        indices[i + 0] = 0 + offset;
        indices[i + 1] = 1 + offset;
        indices[i + 2] = 2 + offset;

        indices[i + 3] = 2 + offset;
        indices[i + 4] = 3 + offset;
        indices[i + 5] = 0 + offset;

        offset += 4;
    }
    IndexBuffer ib(indices.data(), max_index_count);
    vertex_array.set_index_buffer(std::move(ib));
}

void Renderer::flush() noexcept
{
    vertex_array.bind();

    glDrawElements(
        GL_TRIANGLES, batch_data.index_count, GL_UNSIGNED_INT, nullptr);

    batch_data.index_count = 0;
}
