#include "renderer/OrthographicCamera.hpp"

#include <glm/gtc/matrix_transform.hpp>

OrthographicCamera::OrthographicCamera(float left,
                                       float right,
                                       float bottom,
                                       float top) noexcept
    : projection_matrix(glm::ortho(left, right, bottom, top, -1.0f, 1.0f)),
      view_matrix(1.0f),
      view_projection_matrix(projection_matrix * view_matrix)
{}

void OrthographicCamera::set_projection(float left,
                                        float right,
                                        float bottom,
                                        float top) noexcept
{
    projection_matrix      = glm::ortho(left, right, bottom, top, -1.0f, 1.0f);
    view_projection_matrix = projection_matrix * view_matrix;
}

void OrthographicCamera::set_position(const glm::vec3& position) noexcept
{
    this->position = position;
    recalculate_view_matrix();
}

void OrthographicCamera::set_rotation(float rotation) noexcept
{
    this->rotation = rotation;
    recalculate_view_matrix();
}

const glm::vec3& OrthographicCamera::get_position() const noexcept
{
    return position;
}

float OrthographicCamera::get_rotation() const noexcept
{
    return rotation;
}

const glm::mat4& OrthographicCamera::get_projection_matrix() const noexcept
{
    return projection_matrix;
}

const glm::mat4& OrthographicCamera::get_view_matrix() const noexcept
{
    return view_matrix;
}

const glm::mat4& OrthographicCamera::get_view_projection_matrix() const noexcept
{
    return view_projection_matrix;
}

void OrthographicCamera::recalculate_view_matrix() noexcept
{
    glm::mat4 transform = glm::translate(glm::mat4(1.0f), position) *
                          glm::rotate(glm::mat4(1.0f),
                                      glm::radians(rotation),
                                      glm::vec3(0.0f, 0.0f, 1.0f));

    view_matrix            = glm::inverse(transform);
    view_projection_matrix = projection_matrix * view_matrix;
}
