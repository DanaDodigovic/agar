#ifndef RENDERER_SHADER_HPP
#define RENDERER_SHADER_HPP

#include <glm/glm.hpp>

#include <filesystem>
#include <unordered_map>

class ShaderProgram {
  private:
    unsigned program_id;
    std::unordered_map<std::string, unsigned> uniform_location_cache;

  public:
    ShaderProgram(const std::filesystem::path& vertex_shader_filename,
                  const std::filesystem::path& fragment_shader_filename);
    ~ShaderProgram() noexcept;

    ShaderProgram(const ShaderProgram& other) = delete;
    ShaderProgram& operator=(const ShaderProgram& other) = delete;

    ShaderProgram(ShaderProgram&& other) noexcept;
    ShaderProgram& operator=(ShaderProgram&& other) noexcept;

    void bind() const noexcept;
    void unbind() const noexcept;

    void set_uniform4f(std::string_view name, const glm::vec4& value) noexcept;
    void set_uniform1i(std::string_view name, int value) noexcept;
    void set_uniform_mat4(std::string_view name,
                          const glm::mat4& value) noexcept;

  private:
    int get_uniform_location(std::string_view name) noexcept;
};

class ShaderError : public std::runtime_error {
  public:
    explicit ShaderError(const std::string& message) noexcept;
    explicit ShaderError(const char* message) noexcept;
};

class ShaderLibrary {
  private:
    std::unordered_map<std::string, ShaderProgram> shaders;

  public:
    void add(std::string name, ShaderProgram shader) noexcept;
    ShaderProgram& get(const std::string& name) noexcept;
};

#endif
