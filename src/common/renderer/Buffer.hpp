#ifndef RENDERER_BUFFER_HPP
#define RENDERER_BUFFER_HPP

#include <vector>
#include <string>

/** Defines supported shader data types. */
enum class ShaderDataType {
    None,
    Float,
    Float2,
    Float3,
    Float4,
    Mat3,
    Mat4,
    Int,
    Int2,
    Int3,
    Int4,
    Bool
};

/** Determines the size in bytes of a provided supported shader data type. */
std::size_t shader_data_type_size(ShaderDataType type) noexcept;

/** Abstraction of a single element (e.g. position, texture coordinates, etc.)
 * in a vertex buffer. In OpenGL, this is referred to as attribute. */
class BufferElement {
  public:
    std::string name    = "";
    ShaderDataType type = ShaderDataType::None;
    unsigned size       = 0;
    unsigned offset     = 0;
    bool normalized     = false;

  public:
    BufferElement() = default;
    BufferElement(ShaderDataType type,
                  std::string name,
                  bool normalized = false) noexcept;

    unsigned get_component_count() const noexcept;
};

class BufferLayout {
  private:
    std::vector<BufferElement> elements;
    unsigned stride = 0;

  public:
    BufferLayout() = default;
    BufferLayout(std::initializer_list<BufferElement> elements) noexcept;

    const std::vector<BufferElement>& get_elements() const noexcept;
    unsigned get_stride() const noexcept;

    std::vector<BufferElement>::iterator begin() noexcept;
    std::vector<BufferElement>::iterator end() noexcept;
    std::vector<BufferElement>::const_iterator begin() const noexcept;
    std::vector<BufferElement>::const_iterator end() const noexcept;

  private:
    void calculate_offsets_and_stride() noexcept;
};

class VertexBuffer {
  private:
    unsigned renderer_id;
    BufferLayout layout;

  public:
    VertexBuffer(const float* vertices,
                 unsigned count,
                 BufferLayout layout) noexcept;
    ~VertexBuffer() noexcept;

    VertexBuffer(const VertexBuffer& other) = delete;
    VertexBuffer& operator=(const VertexBuffer& other) = delete;

    VertexBuffer(VertexBuffer&& other) noexcept;
    VertexBuffer& operator=(VertexBuffer&& other) noexcept;

    const BufferLayout& get_layout() const noexcept;

    void update_buffer_data(const float* vertices,
                            unsigned count) const noexcept;

    void bind() const noexcept;
    void unbind() const noexcept;
};

class IndexBuffer {
  private:
    unsigned renderer_id;
    unsigned count;

  public:
    IndexBuffer(const unsigned* indices, unsigned count) noexcept;
    ~IndexBuffer() noexcept;

    IndexBuffer(const IndexBuffer& other) = delete;
    IndexBuffer& operator=(const IndexBuffer& other) = delete;

    IndexBuffer(IndexBuffer&& other) noexcept;
    IndexBuffer& operator=(IndexBuffer&& other) noexcept;

    unsigned get_count() const noexcept;

    void bind() const noexcept;
    void unbind() const noexcept;
};

#endif
