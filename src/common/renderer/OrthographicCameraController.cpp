#include "renderer/OrthographicCameraController.hpp"

#include "Key.hpp"

OrthographicCameraController::OrthographicCameraController(
    const Input& input, float aspect_ratio, bool rotation_enabled) noexcept
    : input(input),
      aspect_ratio(aspect_ratio),
      camera(-aspect_ratio * zoom_level,
             aspect_ratio * zoom_level,
             -zoom_level,
             zoom_level),
      rotation_enabled(rotation_enabled)
{}

void OrthographicCameraController::on_update(Timestep timestep) noexcept
{
    constexpr static float camera_translation_speed = 1.5f;
    constexpr static float camera_rotation_speed    = 90.0f;

    // Horizontal movement keys.
    if (input.is_pressed(Key::Left) || input.is_pressed(Key::H) ||
        input.is_pressed(Key::A)) {
        camera_position.x -= camera_translation_speed * timestep;
    }
    if (input.is_pressed(Key::Right) || input.is_pressed(Key::L) ||
        input.is_pressed(Key::D)) {
        camera_position.x += camera_translation_speed * timestep;
    }

    // Vertical movement keys.
    if (input.is_pressed(Key::Up) || input.is_pressed(Key::K) ||
        input.is_pressed(Key::W)) {
        camera_position.y += camera_translation_speed * timestep;
    }
    if (input.is_pressed(Key::Down) || input.is_pressed(Key::J) ||
        input.is_pressed(Key::S)) {
        camera_position.y -= camera_translation_speed * timestep;
    }

    if (rotation_enabled) {
        // Rotation movement keys.
        if (input.is_pressed(Key::Q)) {
            camera_rotation += camera_rotation_speed * timestep;
        }
        if (input.is_pressed(Key::E)) {
            camera_rotation -= camera_rotation_speed * timestep;
        }

        camera.set_rotation(camera_rotation);
    }

    camera.set_position(camera_position);
}

void OrthographicCameraController::set_position(glm::vec2 position)
{
    camera_position = {position.x, position.y, 0.0f};
    camera.set_position(camera_position);
}

void OrthographicCameraController::on_event(Event& event) noexcept
{
    EventDispatcher dispatcher(event);
    dispatcher.dispatch<MouseScrolledEvent>(
        [this](MouseScrolledEvent& event) { return on_mouse_scroll(event); });
    dispatcher.dispatch<WindowResizeEvent>(
        [this](WindowResizeEvent& event) { return on_window_resize(event); });
}

const OrthographicCamera&
OrthographicCameraController::get_camera() const noexcept
{
    return camera;
}

bool OrthographicCameraController::on_mouse_scroll(
    MouseScrolledEvent& event) noexcept
{
    constexpr static float camera_zoom_speed = 0.05f;
    constexpr static float max_zoom = 0.125f; // Do not allow infinite zoom.

    zoom_level -= event.get_y_offset() * camera_zoom_speed;
    zoom_level = std::max(zoom_level, max_zoom);
    camera.set_projection(-aspect_ratio * zoom_level,
                          aspect_ratio * zoom_level,
                          -zoom_level,
                          zoom_level);
    return false;
}

bool OrthographicCameraController::on_window_resize(
    WindowResizeEvent& event) noexcept
{
    aspect_ratio = static_cast<float>(event.get_width()) / event.get_height();
    camera.set_projection(-aspect_ratio * zoom_level,
                          aspect_ratio * zoom_level,
                          -zoom_level,
                          zoom_level);
    return false;
}
