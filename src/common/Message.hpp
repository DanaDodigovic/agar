#include <array>
#include <string>
#include <ostream>

/* Message that may be written to and read from the socket (wire) since it
 * consists of a standard fixed length header that defines number of bytes in
 * the message body and the message body itself. */
class Message {
  public:
    /* Fixed length message header that defines message body length. */
    class Header {
      public:
        // Total number of bytes in the header.
        constexpr static std::size_t size = 8;

      private:
        std::array<char, size> data;

      public:
        Header() = default;
        explicit Header(std::size_t body_size) noexcept;

        std::array<char, size>& get_data() noexcept;
        std::size_t decode_body_size() const;

      private:
        std::string_view get_body_size() const noexcept;

        friend std::ostream& operator<<(std::ostream& os, const Header& h);
    };

  public:
    Header header;
    std::string body;

  public:
    Message() = default;
    explicit Message(std::string body) noexcept;

    friend std::ostream& operator<<(std::ostream& os, const Message& m);
};
