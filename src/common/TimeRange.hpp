#ifndef TIMERANGE_HPP
#define TIMERANGE_HPP

#include <ostream>

class TimeRange {
  private:
    long start;
    long end;

  public:
    TimeRange(long start, long end) noexcept;

    long get_start() const noexcept;
    long get_end() const noexcept;

    /* Returns true if this time range contains this timepoint. */
    bool contains_timepoint(long timepoint,
                            bool allow_equal = true) const noexcept;

    /* Returns true if this time range is contained with other (other contains
     * both start and end points of this range).*/
    bool is_within(const TimeRange& other) const noexcept;

    /* Returns true if this time range overlaps with other. This is true if one
     * of the points of one range is contained within the other range, but the
     * other point is not contained. */
    bool is_overlapping(const TimeRange& other) const noexcept;

    bool operator==(const TimeRange& other) const noexcept;
    bool operator!=(const TimeRange& other) const noexcept;
    bool operator<(const TimeRange& other) const noexcept;

    friend std::ostream& operator<<(std::ostream& os, const TimeRange& r);
};

#endif
