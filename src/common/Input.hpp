#ifndef INPUT_HPP
#define INPUT_HPP

#include "Key.hpp"
#include "Window.hpp"

/* Facilitates input polling. */
class Input {
  private:
    const Window& window;

  public:
    explicit Input(const Window& window) noexcept;

    bool is_pressed(Key key) const noexcept;
    bool is_pressed(MouseButton button) const noexcept;

    double get_mouse_x() const noexcept;
    double get_mouse_y() const noexcept;

    std::pair<double, double> get_mouse_position() const noexcept;
};

#endif
