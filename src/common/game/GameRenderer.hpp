#ifndef GAME_RENDERER_HPP
#define GAME_RENDERER_HPP

#include "renderer/Renderer.hpp"
#include "game/Game.hpp"
#include "Window.hpp"
#include "renderer/OrthographicCameraController.hpp"

class GameRenderer {
  protected:
    Renderer renderer;
    const Game::State& state;


  public:
    GameRenderer(const FilePathConfiguration& path_config,
                 const Game::State& state) noexcept;
    virtual ~GameRenderer() = default;

    void draw_border() noexcept;
    void draw_player() noexcept;
    void draw_food() noexcept;
    void render_score(const Window& window, const std::string& name) noexcept;

    void draw_scene(const OrthographicCameraController& camera_controller,
                    const Window& window,
                    const std::string& name = "") noexcept;

    virtual void render_gui(const Window& window,
                            const std::string& name) noexcept = 0;

    Renderer& get_renderer();
};

#endif
