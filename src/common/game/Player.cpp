#include "Player.hpp"
#include "Random.hpp"
#include <iostream>

Player::Player()
{
    static std::uniform_real_distribution<float> dist(0.3f, 0.9f);
    color = {dist(Random::generator()),
             dist(Random::generator()),
             dist(Random::generator()),
             dist(Random::generator())};
}

Player::Player(std::string name, glm::vec4 color) noexcept
    : name(std::move(name)), color(std::move(color))
{
    static std::uniform_real_distribution<float> distp(-1.0f, 1.0f);
    center = {distp(Random::generator()), distp(Random::generator())};
}

Player::Player(std::string name) noexcept : name(std::move(name))
{
    static std::uniform_real_distribution<float> dist(0.3f, 0.9f);
    color = {dist(Random::generator()),
             dist(Random::generator()),
             dist(Random::generator()),
             dist(Random::generator())};

    static std::uniform_real_distribution<float> distp(-1.0f, 1.0f);
    center = {distp(Random::generator()), distp(Random::generator())};
}

glm::vec2 Player::get_window_relative_center(glm::vec2 window_dimensions) const
{
    float width  = window_dimensions.x;
    float height = window_dimensions.y;

    return {center.x * width / 2.0 + width / 2.0,
            -center.y * height / 2.0 + height / 2.0};
}

std::ostream& operator<<(std::ostream& os, const Player& player)
{
    os << "[name: " << player.name << ", center: " << player.center
       << ", radius: " << player.radius << ", color: " << player.color
       << ", score: " << player.score << ", speed: " << player.speed << "]";
    return os;
}

bool operator==(const Player& first, const Player& second)
{
    return first.name == second.name && first.center == second.center &&
           first.radius == second.radius && first.color == second.color &&
           first.score == second.score && first.speed == second.speed;
}
