#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Vector.hpp"

#include <glm/glm.hpp>

#include <string>
#include <ostream>

struct Player {
    std::string name;
    glm::vec2 center = {0.0f, 0.0f};
    float radius     = 0.1f;
    glm::vec4 color;
    int score   = 10;
    float speed = 0.5f;

    std::size_t n_eaten = 0;

    Player();
    Player(std::string name) noexcept;
    Player(std::string name, glm::vec4 color) noexcept;

    glm::vec2 get_window_relative_center(glm::vec2 window_dimensions) const;

    friend std::ostream& operator<<(std::ostream& os, const Player& player);
    friend bool operator==(const Player& first, const Player& second);
};

#endif
