#ifndef PLAYER_CONTROLLER
#define PLAYER_CONTROLLER

#include "Input.hpp"
#include "Timestep.hpp"
#include "game/Game.hpp"

class PlayerController {
  public:
    Player& player;
    PlayableRange playable_range;

  public:
    PlayerController(Player& player, PlayableRange playable_range);

    void on_update(Timestep timestep, glm::vec2 movement_command) noexcept;

  private:
    void move_player(glm::vec2 delta) noexcept;
};

#endif
