#include "TimeRange.hpp"

#include "Assert.hpp"

TimeRange::TimeRange(long start, long end) noexcept : start(start), end(end)
{
    ASSERT_FMT(
        start <= end,
        "Cannot create time range with start greater than end: {{{}, {}}}.",
        start,
        end);
}

long TimeRange::get_start() const noexcept
{
    return start;
}

long TimeRange::get_end() const noexcept
{
    return end;
}

bool TimeRange::contains_timepoint(long timepoint,
                                   bool allow_equal) const noexcept
{
    return allow_equal ? (timepoint >= start && timepoint <= end) :
                         (timepoint > start && timepoint < end);
}

bool TimeRange::is_within(const TimeRange& other) const noexcept
{
    return other.contains_timepoint(start) && other.contains_timepoint(end);
}

bool TimeRange::is_overlapping(const TimeRange& other) const noexcept
{
    return
        //              ----this---
        // -------other-------
        (other.contains_timepoint(start, false) &&
         !other.contains_timepoint(end)) ||
        // ----this---
        //      -------other-------
        (!other.contains_timepoint(start) &&
         other.contains_timepoint(end, false)) ||
        //              ----other---
        // -------this-------
        (contains_timepoint(other.start, false) &&
         !contains_timepoint(other.end)) ||
        // ----other---
        //      -------this-------
        (!contains_timepoint(other.start) &&
         contains_timepoint(other.end, false));
}

bool TimeRange::operator==(const TimeRange& other) const noexcept
{
    return start == other.start && end == other.end;
}

bool TimeRange::operator!=(const TimeRange& other) const noexcept
{
    return !(*this == other);
}

bool TimeRange::operator<(const TimeRange& other) const noexcept
{
    return end == other.end ? start < other.start : end < other.end;
}

std::ostream& operator<<(std::ostream& os, const TimeRange& tr)
{
    return os << '{' << tr.start << ", " << tr.end << '}';
}
