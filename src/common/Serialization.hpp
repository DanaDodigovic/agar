#ifndef SERIALIATION_HPP
#define SERIALIATION_HPP

#include "game/Game.hpp"
#include "Key.hpp"

#include <sstream>

#include <glm/glm.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

namespace Serialization {
/* Deserializes provided data. */
template<typename T>
T deserialize(const std::string& serialized_data)
{
    static_assert(std::is_default_constructible_v<T>,
                  "Deserialization type must be default constructible.");
    T data;
    {
        std::istringstream archive_stream(serialized_data);
        boost::archive::binary_iarchive archive(archive_stream);
        archive >> data;
    }
    return data;
}

/* Serializes provided data. */
template<typename T>
std::string serialize(const T& data)
{
    std::ostringstream data_archive_stream;
    {
        boost::archive::binary_oarchive data_archive(data_archive_stream);
        data_archive << data;
    }
    return data_archive_stream.str();
}

}

namespace boost::serialization {

template<class Archive>
void serialize(Archive& ar, glm::vec2& v, const unsigned int /*version*/)
{
    ar& v.x;
    ar& v.y;
}

template<class Archive>
void serialize(Archive& ar, glm::vec4& v, const unsigned int /*version*/)
{
    ar& v.r;
    ar& v.g;
    ar& v.b;
    ar& v.a;
}

template<class Archive>
void serialize(Archive& ar, Player& p, const unsigned int /*version*/)
{
    ar& p.name;
    ar& p.center;
    ar& p.radius;
    ar& p.color;
    ar& p.score;
    ar& p.speed;
}

template<class Archive>
void serialize(Archive& ar, FoodInstance& f, const unsigned int /*version*/)
{
    ar& f.center;
    ar& f.color;
}

template<class Archive>
void serialize(Archive& ar, Key& k, const unsigned int /*version*/)
{
    ar& k;
}

template<class Archive>
void serialize(Archive& ar, Food& f, const unsigned int /*version*/)
{
    ar& f.n_instances;
    ar& f.radius;
    ar& f.playable_range;
    ar& f.food_instances;
}

template<class Archive>
void serialize(Archive& ar, Game::State& s, const unsigned int /*version*/)
{
    ar& s.border_size;
    ar& s.players;
    ar& s.food;
}

template<class Archive>
void serialize(Archive& ar, PlayableRange& p, const unsigned int /*version*/)
{
    ar& p.min_x;
    ar& p.max_x;
    ar& p.min_y;
    ar& p.max_y;
}

} // namespace serialization

#endif
