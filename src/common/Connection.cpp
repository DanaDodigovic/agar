#include "Connection.hpp"

#include <fmt/format.h>

Connection::Connection(const Executor& executor) : socket(executor) {}

Connection::Socket& Connection::get_socket() noexcept
{
    return socket;
}

void Connection::async_write(std::string message, WriteHandler handler)
{
    // Create shared pointer to this connection object so that it can be passed
    // by value to the callback lambda below. This ensures that the object is
    // kept alive by the callback lambda so it cannot happen that the connection
    // object goes out of scope before the callback is called.
    auto self = shared_from_this();

    // Construct outbound message.
    outbound_wire_message = Message(std::move(message));

    // Write the message to the socket. Gather-write is used to send both the
    // message header, action and body in a single write operation.
    const std::array<boost::asio::const_buffer, 2> buffers = {
        boost::asio::buffer(outbound_wire_message.header.get_data()),
        boost::asio::buffer(outbound_wire_message.body)};

    // Calculate number of bytes that need to be written to the socket.
    std::size_t bytes_to_write = 0;
    for (const auto& buffer : buffers) { bytes_to_write += buffer.size(); }

    // Issue asynchronous write operation to write the buffers to the socket.
    boost::asio::async_write(
        socket,
        buffers,
        [self, handler = std::move(handler), bytes_to_write](
            const ErrorCode& e, std::size_t written) {
            if (written != bytes_to_write) {
                throw std::runtime_error(fmt::format(
                    "Failed to write correct amount of bytes to the socket "
                    "(expected {} bytes, but {} were written) Error: {}.",
                    bytes_to_write,
                    written,
                    e.message()));
            }
            if (e) {
                throw std::runtime_error(fmt::format(
                    "Failed to write the data to the socket: {}", e.message()));
            }
            handler(self);
        });
}

void Connection::async_read(ReadHandler handler)
{
    // Create shared pointer to this connection object so that it can be
    // passed by value to the callback lambda below. This ensures that the
    // object is kept alive by the callback lambda so it cannot happen that
    // the connection object goes out of scope before the callback is
    // called.
    auto self = shared_from_this();

    // Issue asynchronous read operation to read the fixed length header.
    boost::asio::async_read(
        socket,
        boost::asio::buffer(inbound_wire_message.header.get_data()),
        [self, handler = std::move(handler)](const ErrorCode& e,
                                             std::size_t received) mutable {
            if (e == boost::asio::error::eof) {
                throw std::runtime_error("Connection has been closed.");
            }
            if (received != Message::Header::size) {
                throw std::runtime_error(fmt::format(
                    "Failed to read message header (expected {} bytes, but {} "
                    "bytes were read). Error: {}.",
                    Message::Header::size,
                    received,
                    e ? e.message() : "No extra error message"));
            }
            if (e) {
                throw std::runtime_error(fmt::format(
                    "Failed to read message header: {}", e.message()));
            }
            self->handle_read_header(std::move(handler));
        });
}

void Connection::write(std::string message)
{
    // Construct outbound message.
    outbound_wire_message = Message(std::move(message));

    // Write the message to the socket. Gather-write is used to send both the
    // message header, action and body in a single write operation.
    const std::array<boost::asio::const_buffer, 2> buffers = {
        boost::asio::buffer(outbound_wire_message.header.get_data()),
        boost::asio::buffer(outbound_wire_message.body)};

    // Calculate number of bytes that need to be written to the socket.
    std::size_t bytes_to_write = 0;
    for (const auto& buffer : buffers) { bytes_to_write += buffer.size(); }

    // Issue synchronous write operation to write the buffers to the socket.
    ErrorCode e;
    boost::asio::write(socket, buffers, e);
    if (e) {
        throw std::runtime_error(fmt::format(
            "Failed to write the data to the socket: {}", e.message()));
    }
}

std::string Connection::read()
{
    // Issue synchronous read operation to read the fixed length header.
    ErrorCode e;
    boost::asio::read(
        socket, boost::asio::buffer(inbound_wire_message.header.get_data()), e);
    if (e == boost::asio::error::eof) {
        throw std::runtime_error("Connection has been closed.");
    }
    if (e) {
        throw std::runtime_error(
            fmt::format("Failed to read message header: {}", e.message()));
    }

    // Inbound message body size. This is the number of bytes that needs to be
    // read from the socket.
    auto inbound_body_size = inbound_wire_message.header.decode_body_size();

    // Reserve space for inbound message body.
    inbound_wire_message.body.resize(inbound_body_size);

    // Start a synchronous call to receive the message action and body.
    boost::asio::read(
        socket, boost::asio::buffer(inbound_wire_message.body), e);
    if (e) {
        throw std::runtime_error(fmt::format(
            "Failed to read message body and action: {}", e.message()));
    }

    return std::move(inbound_wire_message.body);
}

void Connection::handle_read_header(ReadHandler handler)
{
    // Create shared pointer to this connection object so that it can be
    // passed by value to the callback lambda below. This ensures that the
    // object is kept alive by the callback lambda so it cannot happen that
    // the connection object goes out of scope before the callback is
    // called.
    auto self = shared_from_this();

    // Inbound message body size. This is the number of bytes that needs to be
    // read from the socket.
    auto inbound_body_size = inbound_wire_message.header.decode_body_size();

    // Reserve space for inbound message body.
    inbound_wire_message.body.resize(inbound_body_size);

    // Start an asynchronous call to receive the message body and action.
    boost::asio::async_read(
        socket,
        boost::asio::buffer(inbound_wire_message.body),
        [self, handler = std::move(handler), inbound_body_size](
            const ErrorCode& e, std::size_t received) {
            if (received != inbound_body_size) {
                throw std::runtime_error(fmt::format(
                    "Failed to read message body and action (expected {} "
                    "bytes, but {} bytes were read). Error: {}.",
                    inbound_body_size,
                    received,
                    e ? e.message() : "No extra error message"));
            }
            if (e) {
                throw std::runtime_error(fmt::format(
                    "Failed to read message body and action: {}", e.message()));
            }

            // Consume inbound message (move the body to the provided handler).
            handler(self, std::move(self->inbound_wire_message.body));
        });
}
