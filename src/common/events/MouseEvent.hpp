#ifndef MOUSEEVENT_HPP
#define MOUSEEVENT_HPP

#include "events/Event.hpp"

class MouseMovedEvent : public Event {
  private:
    double x; // Mouse X offset (horizontal scrolling).
    double y; // Mouse Y offset (vertical scrolling).

  public:
    MouseMovedEvent(double x, double y) noexcept;

    double get_x() const noexcept;
    double get_y() const noexcept;

    std::string get_information() const noexcept override;

    static Type get_static_type() noexcept;
};

class MouseScrolledEvent : public Event {
  private:
    double x_offset; // Mouse X offset (horizontal scrolling).
    double y_offset; // Mouse Y offset (vertical scrolling).

  public:
    MouseScrolledEvent(double x_offset, double y_offset) noexcept;

    double get_x_offset() const noexcept;
    double get_y_offset() const noexcept;

    std::string get_information() const noexcept override;

    static Type get_static_type() noexcept;
};

/* Base class for all mouse button events. */
class MouseButtonEvent : public Event {
  private:
    int button; // Mouse button code.

  public:
    int get_button() const noexcept;

  protected:
    /* Mouse button event constructor. Protected visibility is needed to ensure
     * that instance of this class cannot be constructed. Only inheriting
     * classes can invoke this constructor. */
    MouseButtonEvent(Type type, int button) noexcept;
};

class MouseButtonPressedEvent : public MouseButtonEvent {
  public:
    explicit MouseButtonPressedEvent(int button) noexcept;
    std::string get_information() const noexcept override;
    static Type get_static_type() noexcept;
};

class MouseButtonReleasedEvent : public MouseButtonEvent {
  public:
    explicit MouseButtonReleasedEvent(int button) noexcept;
    std::string get_information() const noexcept override;
    static Type get_static_type() noexcept;
};

#endif
