#include "Assert.hpp"
#include "events/Event.hpp"

Event::Event(Type type, Category category) noexcept
    : type(type), category(category)
{}

Event::Type Event::get_type() const noexcept
{
    return type;
}

bool Event::is_in_category(Category category) const noexcept
{
    return static_cast<unsigned>(this->category) &
           static_cast<unsigned>(category);
}

std::string Event::get_name() const noexcept
{
    return type_to_string(get_type());
}

std::string Event::get_information() const noexcept
{
    return get_name();
}

std::string Event::type_to_string(Type type) noexcept
{
    switch (type) {
        case Type::None: return "None";
        case Type::WindowClose: return "WindowClose";
        case Type::WindowResize: return "WindowResize";
        case Type::WindowFocus: return "WindowFocus";
        case Type::WindowLostFocus: return "WindowLostFocus";
        case Type::WindowMoved: return "WindowMoved";
        case Type::AppTick: return "AppTick";
        case Type::AppUpdate: return "AppUpdate";
        case Type::AppRender: return "AppRender";
        case Type::KeyPressed: return "KeyPressed";
        case Type::KeyReleased: return "KeyReleased";
        case Type::KeyTyped: return "KeyTyped";
        case Type::MouseButtonPressed: return "MouseButtonPressed";
        case Type::MouseButtonReleased: return "MouseButtonReleased";
        case Type::MouseMoved: return "MouseMoved";
        case Type::MouseScrolled: return "MouseScrolled";
    }
    ASSERT_UNREACHABLE("Unhandled event type.");
}

std::ostream& operator<<(std::ostream& os, const Event& event)
{
    return os << event.get_information();
}

Event::Category operator|(Event::Category first,
                          Event::Category second) noexcept
{
    return static_cast<Event::Category>(static_cast<unsigned>(first) |
                                        static_cast<unsigned>(second));
}

EventDispatcher::EventDispatcher(Event& event) noexcept : event(event) {}
