#include "events/KeyEvent.hpp"

int KeyEvent::get_keycode() const noexcept
{
    return keycode;
}

KeyEvent::KeyEvent(Type type, int keycode) noexcept
    : Event(type, Category::Keyboard | Category::Input), keycode(keycode)
{}

KeyPressedEvent::KeyPressedEvent(int keycode, int repeat_count) noexcept
    : KeyEvent(get_static_type(), keycode), repeat_count(repeat_count)
{}

unsigned KeyPressedEvent::get_repeat_count() const noexcept
{
    return repeat_count;
}

std::string KeyPressedEvent::get_information() const noexcept
{
    return fmt::format(
        "{}: {} ({} repeats)", get_name(), get_keycode(), repeat_count);
}

Event::Type KeyPressedEvent::get_static_type() noexcept
{
    return Type::KeyPressed;
}

KeyReleasedEvent::KeyReleasedEvent(int keycode) noexcept
    : KeyEvent(get_static_type(), keycode)
{}

std::string KeyReleasedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}", get_name(), get_keycode());
}

Event::Type KeyReleasedEvent::get_static_type() noexcept
{
    return Type::KeyReleased;
}

KeyTypedEvent::KeyTypedEvent(int keycode) noexcept
    : KeyEvent(get_static_type(), keycode)
{}

std::string KeyTypedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}", get_name(), get_keycode());
}

Event::Type KeyTypedEvent::get_static_type() noexcept
{
    return Type::KeyTyped;
}
