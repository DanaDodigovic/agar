#ifndef APPLICATIONEVENT_HPP
#define APPLICATIONEVENT_HPP

#include "events/Event.hpp"

class WindowResizeEvent : public Event {
  private:
    unsigned width;
    unsigned height;

  public:
    WindowResizeEvent(unsigned width, unsigned height) noexcept;

    unsigned get_width() const noexcept;
    unsigned get_height() const noexcept;

    std::string get_information() const noexcept override;

    static Type get_static_type() noexcept;
};

class WindowCloseEvent : public Event {
  public:
    WindowCloseEvent() noexcept;
    static Type get_static_type() noexcept;
};

class AppTickEvent : public Event {
  public:
    AppTickEvent() noexcept;
    static Type get_static_type() noexcept;
};

class AppUpdateEvent : public Event {
  public:
    AppUpdateEvent() noexcept;
    static Type get_static_type() noexcept;
};

class AppRenderEvent : public Event {
  public:
    AppRenderEvent() noexcept;
    static Type get_static_type() noexcept;
};

#endif
