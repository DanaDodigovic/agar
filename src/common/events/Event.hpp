#ifndef EVENTS_HPP
#define EVENTS_HPP

#include "Utility.hpp"

#include <string>
#include <functional>

/* All events are currently blocking, meaning when an event occurs it
 * immediately gets dispatched and must be dealt with right then and there. For
 * the future, a * better strategy might be to buffer events in an event bus and
 * process then during the "event" part of the update stage. */

/** Base class for all events. */
class Event {
  public:
    enum class Type : unsigned {
        None = 0,
        WindowClose,
        WindowResize,
        WindowFocus,
        WindowLostFocus,
        WindowMoved,
        AppTick,
        AppUpdate,
        AppRender,
        KeyPressed,
        KeyReleased,
        KeyTyped,
        MouseButtonPressed,
        MouseButtonReleased,
        MouseMoved,
        MouseScrolled
    };

    enum class Category : unsigned {
        Application = bitfield(0),
        Input       = bitfield(1),
        Keyboard    = bitfield(2),
        Mouse       = bitfield(3),
        MouseButton = bitfield(4)
    };

  public:
    bool handled = false;

  private:
    Type type;
    Category category;

  public:
    Event(Type type, Category category) noexcept;
    virtual ~Event() = default;

    Type get_type() const noexcept;
    unsigned get_category_flags() const noexcept;

    bool is_in_category(Category category) const noexcept;

    std::string get_name() const noexcept;
    virtual std::string get_information() const noexcept;

    static std::string type_to_string(Type type) noexcept;

    friend std::ostream& operator<<(std::ostream& os, const Event& event);
};

Event::Category operator|(Event::Category first,
                          Event::Category second) noexcept;

class EventDispatcher {
  private:
    Event& event;

  public:
    explicit EventDispatcher(Event& event) noexcept;

    /* Provided type must be an event type that provides a static function that
     * returns event type. This pattern is used to avoid dynamic casting - if
     * type (defined by the enum) provided by the instance is equal to the type
     * provided provided by the static function inside event class, the cast can
     * be performed safely without resorting to the dynamic cast. */
    template<typename EventType>
    bool dispatch(std::function<bool(EventType&)> handler);
};

template<typename EventType>
bool EventDispatcher::dispatch(std::function<bool(EventType&)> handler)
{
    // If event is of requested type, safely static cast the event to the
    // requested type (without dynamic cast) and call the handler.
    if (event.get_type() == EventType::get_static_type()) {
        ASSERT(dynamic_cast<EventType*>(&event),
               "Event types match, but the dynamic cast has failed.");
        EventType& e  = *(static_cast<EventType*>(&event));
        event.handled = handler(e);
        return true;
    }
    return false;
}

#endif
