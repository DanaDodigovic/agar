#include "Message.hpp"

#include <cstring>
#include <charconv>

#include <fmt/format.h>

namespace {
std::size_t hex_decode(std::string_view hex_data);
}

Message::Header::Header(std::size_t body_size) noexcept
{
    auto body_size_str = fmt::format("{:{}x}", body_size, size);
    std::memcpy(data.data(), body_size_str.data(), size);
}

std::array<char, Message::Header::size>& Message::Header::get_data() noexcept
{
    return data;
}

std::size_t Message::Header::decode_body_size() const
{
    return hex_decode(get_body_size());
}

std::string_view Message::Header::get_body_size() const noexcept
{
    return std::string_view(data.data(), size);
}

std::ostream& operator<<(std::ostream& os, const Message::Header& h)
{
    return os << '{' << h.decode_body_size() << '}';
}

std::ostream& operator<<(std::ostream& os, const Message& m)
{
    return os << '{' << m.header << ", " << m.body << '}';
}

Message::Message(std::string body) noexcept
    : header(body.size()), body(std::move(body))
{}

namespace {
std::size_t hex_decode(std::string_view hex_data)
{
    // Remove leading whitespace since std::from_chars does not skip whitespace.
    std::string_view no_whitespace_hex_data =
        hex_data.substr((hex_data.find_first_not_of(" \t")));

    // Parse hex data into an integer value.
    std::size_t ret;
    auto [ptr, err] = std::from_chars(
        no_whitespace_hex_data.begin(), no_whitespace_hex_data.end(), ret, 16);

    // Make sure no error has occurred when parsing.
    if (err != std::errc()) {
        throw std::runtime_error(
            fmt::format("Failed to decode hex string: {}", hex_data));
    }

    // Make sure whole hex string has been parsed.
    if (ptr != hex_data.data() + hex_data.size()) {
        throw std::runtime_error(fmt::format(
            "The whole hex string '{}' was not parsed! Leftover: {}",
            hex_data,
            std::string_view(ptr, hex_data.data() + hex_data.size() - ptr)));
    }

    return ret;
}
}
