#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include "FilePathConfiguration.hpp"

using VariablesMap       = boost::program_options::variables_map;
using OptionsDescription = boost::program_options::options_description;

enum class KeyboardOption { Arrows, Letters };

class Configuration {
  public:
    FilePathConfiguration path_config;
    std::string player_name;
    KeyboardOption keyboard_option;
    int polling_rate;

  public:
    Configuration(const VariablesMap& variables_map);
};

#endif
