#include "Configuration.hpp"

#include <fmt/core.h>
#include <fmt/ostream.h>

Configuration::Configuration(const VariablesMap& variables_map)
    : path_config(variables_map)
{
    if (variables_map.count("name")) {
        player_name = variables_map["name"].as<std::string>();
        fmt::print("Player name was set to {}\n", player_name);
    } else {
        fmt::print("Player name was not set.\n");
    }

    std::string keyboard;
    if (variables_map.count("keyboard_option")) {
        keyboard = variables_map["keyboard_option"].as<std::string>();
        fmt::print("Keyboard option was set to {}\n", keyboard);
        if (keyboard == "letters") {
            keyboard_option = KeyboardOption::Letters;
        } else { // Default option.
            keyboard_option = KeyboardOption::Arrows;
        }
    } else {
        fmt::print("Keyboard option was not set.\n");
    }

    if (variables_map.count("polling_rate")) {
        polling_rate = variables_map["polling_rate"].as<int>();
        fmt::print("Polling rate was set to {}\n", polling_rate);
    } else {
        fmt::print("Polling rate was not set.\n");
    }
}
