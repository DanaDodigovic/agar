#version 460 core

layout(location = 0) in vec3  position;
layout(location = 1) in vec3  center;
layout(location = 2) in float radius;
layout(location = 3) in vec4  color;

uniform mat4 u_view_projection;
/*uniform mat4 u_transform;*/

out vec2  v_coordinate;
out vec2  v_center;
out float v_radius;
out vec4  v_color;

void main()
{
    gl_Position  = u_view_projection * vec4(position, 1.0);
    v_coordinate = vec2(position.x, position.y);
    v_center     = vec2(center.x, center.y);
    v_radius     = radius;
    v_color      = color;
}
